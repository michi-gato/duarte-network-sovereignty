# Slides template

Slides are at 'public/index.html' to facilitate webhosting on gitlab or the
like. Well, gitlab. But that should be a self-contained folder to host, once
you run `make download_d3js` to download 'https://d3js.org/d3.v5.js'

'reveal.js' is included as a 'git subtree' done like so:

    git remote add revealjs git@github.com:hakimel/reveal.js.git
    git fetch revealjs master
    git subtree add --prefix public/wiring/reveal.js revealjs master --squash

Update with 

    make update_revealjs

That's from https://www.atlassian.com/git/tutorials/git-subtree

Also 'dagrejs' with

    git remote add dagrejs git@github.com:dagrejs/dagre-d3.git
    git fetch dagrejs master
    git subtree add --prefix public/wiring/dagre.js dagrejs master --squash

Update with

    make update_dagrejs

Also 'angularplasmid' with

    git remote add angularplasmid git@github.com:vixis/angularplasmid.git
    git fetch angularplasmid master
    git subtree add --prefix public/wiring/angularplasmid angularplasmid master --squash

Update with

    make update_angularplasmid

Also 'reveald3' with

    git remote add reveald3 git@github.com:gcalmettes/reveal.js-d3.git
    git fetch reveald3 master
    git subtree add --prefix public/wiring/reveald3 reveald3 master --squash

Update with

    make update_reveald3
